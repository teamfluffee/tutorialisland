package scripts.modules.tutorialisland.missions.prayer.decisionnodes;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.prayer.processnodes.WalkToGuide;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldWalkGuide extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) >= 550 &&
                !Constants.PRAYER_AREA.contains(Player.getPosition());
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkToGuide());
        setFalseNode(new ShouldTalkGuide());
    }

}
