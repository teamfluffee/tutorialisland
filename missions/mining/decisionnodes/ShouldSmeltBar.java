package scripts.modules.tutorialisland.missions.mining.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.mining.processnodes.SmeltBar;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldSmeltBar extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 320;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new SmeltBar());
        setFalseNode(new ShouldSmithDagger());
    }

}
