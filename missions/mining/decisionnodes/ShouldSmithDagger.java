package scripts.modules.tutorialisland.missions.mining.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.mining.processnodes.SmithDagger;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldSmithDagger extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 340 ||
                Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 350;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new SmithDagger());
        setFalseNode(new ShouldWalkGate());
    }

}
