package scripts.modules.tutorialisland.missions.gettingstarted.decisionnodes;

import org.tribot.api2007.Game;
import org.tribot.api2007.types.RSTile;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.fluffeesapi.scripting.reusable.nodes.NavigateObject;
import scripts.modules.tutorialisland.data.Constants;
public class ShouldExitRoom extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 10;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new NavigateObject(new RSTile(3098, 3107, 0)));
        setFalseNode(new ShouldOpenSettings());
    }

}
