package scripts.modules.tutorialisland.missions.gettingstarted.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.data.Constants;
import scripts.modules.tutorialisland.missions.gettingstarted.processnodes.HandleOptions;
import scripts.modules.tutorialisland.missions.gettingstarted.processnodes.OpenSettings;

public class ShouldOpenSettings extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 3;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new OpenSettings());
        setFalseNode(new HandleOptions());
    }

}
