package scripts.modules.tutorialisland.missions.gettingstarted.processnodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.GameTab;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.types.RSInterface;
import scripts.modules.fluffeesapi.scripting.entityselector.finders.prefabs.InterfaceEntity;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.modules.fluffeesapi.utilities.Conditions;
import scripts.modules.fluffeesapi.utilities.Utilities;

public class OpenSettings extends SuccessProcessNode {

    private final int INTERFACE = General.isClientResizable() ? 164 : 548, TAB = General.isClientResizable() ? 38 : 35;

    @Override
    public String getStatus() {
        return "Opening Settings";
    }

    @Override
    public void successExecute() {
        RSInterface interfaceChild = Interfaces.get(INTERFACE, TAB);
        if (interfaceChild == null) {
            return;
        }
        if (!Utilities.checkActions(interfaceChild, "Options")) {
            interfaceChild = new InterfaceEntity().inMaster(INTERFACE).actionEquals("Options").getFirstResult();
            if (interfaceChild == null) {
                return;
            }
        }
        if (interfaceChild.click()) {
            Timing.waitCondition(Conditions.tabOpened(GameTab.TABS.OPTIONS), General.random(1000, 1250));
        }
    }
}