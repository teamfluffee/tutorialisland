package scripts.modules.tutorialisland.missions.cook.decisionnodes;

import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.cook.processnodes.WalkToDoor;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldWalkGuide extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !Constants.COOK_AREA.contains(Player.getPosition()) && Objects.findNearest(7, "Door").length < 1;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkToDoor());
        setFalseNode(new ShouldOpenDoor());
    }

}
