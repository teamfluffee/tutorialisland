package scripts.modules.tutorialisland.missions.cook.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.data.Constants;
import scripts.modules.tutorialisland.missions.cook.processnodes.CookBread;
import scripts.modules.tutorialisland.missions.cook.processnodes.MakeBreadDough;

public class ShouldMakeBreadDough extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 150;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new MakeBreadDough());
        setFalseNode(new CookBread());
    }

}