package scripts.modules.tutorialisland.missions.charactersetup.decisionnodes;

import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.FactoryDecisionNode;
import scripts.modules.tutorialisland.data.Variables;
import scripts.modules.tutorialisland.missions.charactersetup.processnodes.PickGender;
import scripts.modules.tutorialisland.missions.charactersetup.processnodes.StyleCharacter;

public class ShouldPickGender extends FactoryDecisionNode {

    private Variables variables;

    @Override
    public boolean isValid() {
        return !variables.hasPickedGender();
    }

    @Override
    public void initializeNode() {
        setTrueNode(new PickGender(variables));
        setFalseNode(new StyleCharacter());
    }

    public static ShouldPickGender create(Variables variables) {
        ShouldPickGender decisionNode = new ShouldPickGender();
        decisionNode.variables = variables;
        decisionNode.initializeNode();
        return decisionNode;

    }
}
