package scripts.modules.tutorialisland.missions.charactersetup.decisionnodes;

import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSVarBit;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.data.Variables;
import scripts.modules.tutorialisland.missions.charactersetup.processnodes.ClaimName;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldClaimName extends ConstructorDecisionNode {

    private Variables variables;

    @Override
    public boolean isValid() {
        return Player.getRSPlayer().getName().contains("#") && RSVarBit.get(Constants.NAME_SET_VARBIT).getValue() == 0;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new ClaimName());
        setFalseNode(ShouldPickGender.create(variables));
    }

    public static ShouldClaimName create(Variables variables) {
        ShouldClaimName node = new ShouldClaimName();
        node.variables = variables;
        node.initializeNode();
        return node;
    }

}
