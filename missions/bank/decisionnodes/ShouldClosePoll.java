package scripts.modules.tutorialisland.missions.bank.decisionnodes;

import org.tribot.api2007.Game;
import org.tribot.api2007.Interfaces;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.bank.processnodes.ClosePoll;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldClosePoll extends ConstructorDecisionNode {

    private final int POLL_BOOTH_MASTER_ID = 310;

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 525 &&
                Interfaces.isInterfaceSubstantiated(POLL_BOOTH_MASTER_ID);
    }

    @Override
    public void initializeNode() {
        setTrueNode(new ClosePoll());
        setFalseNode(new ShouldWalkToGuide());
    }

}
