package scripts.modules.tutorialisland.missions.bank.decisionnodes;

import org.tribot.api2007.Banking;
import org.tribot.api2007.Equipment;
import scripts.modules.fluffeesapi.client.clientextensions.Inventory;
import scripts.modules.fluffeesapi.data.structures.ScriptVariables;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.bank.processnodes.DepositItems;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldDepositItems extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Banking.isBankScreenOpen() &&
                (ScriptVariables.getInstance().getRandomNumber(4) !=
                            Constants.DepositSettings.NONE.getDepositValue() &&
                        (Inventory.getSize() > 0 || Equipment.getItems().length > 0));
    }

    @Override
    public void initializeNode() {
        setTrueNode(new DepositItems());
        setFalseNode(new ShouldCloseBank());
    }

}