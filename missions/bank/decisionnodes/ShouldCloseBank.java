package scripts.modules.tutorialisland.missions.bank.decisionnodes;

import org.tribot.api2007.Banking;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.bank.processnodes.CloseBank;

public class ShouldCloseBank extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Banking.isBankScreenOpen();
    }

    @Override
    public void initializeNode() {
        setTrueNode(new CloseBank());
        setFalseNode(new ShouldOpenPoll());
    }

}
