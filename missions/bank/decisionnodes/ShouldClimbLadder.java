package scripts.modules.tutorialisland.missions.bank.decisionnodes;

import org.tribot.api2007.Player;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.bank.processnodes.ClimbLadder;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldClimbLadder extends ConstructorDecisionNode {
    @Override
    public boolean isValid() {
        return Constants.UNDERGROUND_AREA.contains(Player.getPosition());
    }

    @Override
    public void initializeNode() {
        setTrueNode(new ClimbLadder());
        setFalseNode(new ShouldWalkToBank());
    }
}
