package scripts.modules.tutorialisland.missions.bank.decisionnodes;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.bank.processnodes.WalkToBank;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldWalkToBank extends ConstructorDecisionNode {
    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) < 525 &&
                !Constants.BANK_AREA.contains(Player.getPosition());
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkToBank());
        setFalseNode(new ShouldOpenBank());
    }
}
