package scripts.modules.tutorialisland.missions.bank.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.client.clientextensions.NPCChat;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.bank.processnodes.OpenPoll;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldOpenPoll extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 520 &&
                !NPCChat.inChat(1);
    }

    @Override
    public void initializeNode() {
        setTrueNode(new OpenPoll());
        setFalseNode(new ShouldTalkPoll());
    }

}
