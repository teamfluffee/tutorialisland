package scripts.modules.tutorialisland.missions.bank.processnodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import scripts.modules.fluffeesapi.client.clientextensions.Equipment;
import scripts.modules.fluffeesapi.client.clientextensions.Inventory;
import scripts.modules.fluffeesapi.data.structures.ScriptVariables;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.modules.fluffeesapi.utilities.Conditions;

public class DepositItems extends SuccessProcessNode {

    @Override
    public String getStatus() {
        return "Depositing Items";
    }

    @Override
    public void successExecute() {
        switch (ScriptVariables.getInstance().getRandomNumber(3)) {
            case 0:
                if (Inventory.getSize() > 0) {
                    Banking.depositAll();
                }
                Timing.waitCondition(Conditions.inventoryIsEmpty(), General.random(3000, 5000));
                break;
            case 1:
                if (Equipment.getItems().length > 0) {
                    Banking.depositEquipment();
                }
                Timing.waitCondition(Conditions.nothingEquipped(), General.random(3000, 5000));
                break;
            case 2:
                if (Inventory.getSize() > 0) {
                    Banking.depositAll();
                }
                if (Equipment.getItems().length > 0) {
                    Banking.depositEquipment();
                }
                Timing.waitCondition(Conditions.inventoryIsEmpty(), General.random(3000, 5000));
                Timing.waitCondition(Conditions.nothingEquipped(), General.random(3000, 5000));
                break;

        }
    }
}