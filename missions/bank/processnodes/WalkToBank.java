package scripts.modules.tutorialisland.missions.bank.processnodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSTile;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.modules.fluffeesapi.utilities.Conditions;
import scripts.modules.fluffeesapi.utilities.Utilities;
import scripts.modules.tutorialisland.data.Constants;

public class WalkToBank extends SuccessProcessNode {

    private final RSTile BANK_TILE = Utilities.getRandomizedTile(new RSTile(3121, 3121, 0), 2);

    @Override
    public String getStatus() {
        return "Walking to Bank";
    }

    @Override
    public void successExecute() {
        WebWalking.walkTo(BANK_TILE);
        Timing.waitCondition(Conditions.areaContains(Constants.BANK_AREA), General.random(10000, 15000));
    }
}
