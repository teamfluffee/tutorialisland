package scripts.modules.tutorialisland.missions.combat.decisionnodes;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.combat.processnodes.EnterPit;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldEnterPit extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !Constants.COMBAT_PIT_AREA.contains(Player.getPosition()) &&
                (Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) >= 440 &&
                        Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) <= 460);
    }

    @Override
    public void initializeNode() {
        setTrueNode(new EnterPit());
        setFalseNode(new ShouldMeleeRat());
    }

}
