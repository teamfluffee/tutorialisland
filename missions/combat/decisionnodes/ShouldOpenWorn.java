package scripts.modules.tutorialisland.missions.combat.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.combat.processnodes.OpenWornInterface;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldOpenWorn extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 400;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new OpenWornInterface());
        setFalseNode(new ShouldEquipItem());
    }

}
