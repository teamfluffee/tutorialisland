package scripts.modules.tutorialisland.missions.combat.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.combat.processnodes.MeleeRat;
import scripts.modules.tutorialisland.missions.combat.processnodes.RangeRat;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldMeleeRat extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 450 ||
                Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 460;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new MeleeRat());
        setFalseNode(new RangeRat());
    }

}
