package scripts.modules.tutorialisland.missions.survival.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.survival.processnodes.OpenInventory;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldOpenInventory extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 30;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new OpenInventory());
        setFalseNode(new ShouldCatchFish());
    }

}
