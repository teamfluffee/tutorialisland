package scripts.modules.tutorialisland.missions.survival.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.survival.processnodes.OpenSkills;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldOpenSkills extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 50;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new OpenSkills());
        setFalseNode(new ShouldChopTree());
    }

}
