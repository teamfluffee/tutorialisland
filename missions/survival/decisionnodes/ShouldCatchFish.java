package scripts.modules.tutorialisland.missions.survival.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.tutorialisland.missions.survival.processnodes.CatchFish;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldCatchFish extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 40 ||
                (Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 100);
    }

    @Override
    public void initializeNode() {
        setTrueNode(new CatchFish());
        setFalseNode(new ShouldOpenSkills());
    }

}
