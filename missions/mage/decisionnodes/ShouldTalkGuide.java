package scripts.modules.tutorialisland.missions.mage.decisionnodes;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.fluffeesapi.scripting.reusable.nodes.TalkToGuide;
import scripts.modules.tutorialisland.data.Constants;

public class ShouldTalkGuide extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 620 ||
                Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 640 ||
                Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 660 ||
                Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) == 670;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new TalkToGuide("Magic Instructor",
                new String[]{"Yes.", "No, I'm not planning to do that.", "I'm good, thanks."}));
        setFalseNode(new ShouldOpenMagic());
    }

}
