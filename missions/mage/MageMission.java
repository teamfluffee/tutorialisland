package scripts.modules.tutorialisland.missions.mage;

import org.tribot.api2007.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.tutorialisland.missions.mage.decisionnodes.ShouldLeavePrayer;
import scripts.modules.tutorialisland.data.Constants;

public class MageMission implements TreeMission {

    //Documentation:
    //Setting 281 == 610 when you need to leave the prayer guide
    //Setting 281 == 620 when you need to talk to the magic guide for the first time
    //Setting 281 == 630 when you should open the magic tab
    //Setting 281 == 640 when you should talk tot he magic guide for the second time
    //Setting 281 == 650 when you should cast wind strike
    //Setting 281 == 660 || 670 when you should talk to the magic guide for the last time

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldLeavePrayer();
    }

    @Override
    public String getMissionName() {
        return "Mage mission";
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) >= 610 && !isMissionCompleted();
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(Constants.TUTORIAL_ISLAND_SETTING) >= 700;
    }
}
