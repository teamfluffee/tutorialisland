package scripts.modules.tutorialisland;

import scripts.modules.fluffeesapi.client.clientextensions.Game;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.MissionManager;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.TaskFinish;
import scripts.modules.fluffeesapi.scripting.listeners.clickContinueListener.ClickContinueListener;
import scripts.modules.tutorialisland.data.Variables;
import scripts.modules.tutorialisland.missions.bank.BankMission;
import scripts.modules.tutorialisland.missions.charactersetup.StyleMission;
import scripts.modules.tutorialisland.missions.combat.CombatMission;
import scripts.modules.tutorialisland.missions.cook.CookMission;
import scripts.modules.tutorialisland.missions.gettingstarted.GettingStartedMission;
import scripts.modules.tutorialisland.missions.mage.MageMission;
import scripts.modules.tutorialisland.missions.mining.MiningMission;
import scripts.modules.tutorialisland.missions.prayer.PrayerMission;
import scripts.modules.tutorialisland.missions.quest.QuestMission;
import scripts.modules.tutorialisland.missions.survival.SurvivalMission;

import java.util.LinkedList;

public class TutorialIsland implements MissionManager {

    public static final int QUEST_SETTING = 281;
    private double chatSleepModifier;
    private Variables variables;
    private LinkedList<Mission> missionList;
    private ClickContinueListener clickContinueRunnable;

    public TutorialIsland(String username) {
        this(1.0, username);
    }

    public TutorialIsland(double chatSleepModifier, String username) {
        try {
            this.variables = new Variables(this.getClass());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        this.chatSleepModifier = chatSleepModifier;
        this.variables.setUsername(username);
        this.missionList = new LinkedList<>();
    }

    @Override
    public LinkedList<Mission> getMissions() {
        return missionList;
    }

    @Override
    public boolean shouldLoopMissions() {
        return false;
    }

    @Override
    public void initializeMissionList() {
        if (!missionList.isEmpty()) {
            missionList.clear();
        }
        missionList.add(new StyleMission(variables));
        missionList.add(new GettingStartedMission());
        missionList.add(new SurvivalMission());
        missionList.add(new CookMission());
        missionList.add(new QuestMission());
        missionList.add(new MiningMission());
        missionList.add(new CombatMission());
        missionList.add(new BankMission());
        missionList.add(new PrayerMission());
        missionList.add(new MageMission());
    }

    @Override
    public String getMissionName() {
        return "Tutorial Island";
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(QUEST_SETTING) < 1000;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(QUEST_SETTING) >= 1000;
    }

    @Override
    public boolean shouldStopExecution(TaskFinish childTaskFinish) {
        if (childTaskFinish == StyleMission.MissionFinishes.NAME_UNAVAILABLE && variables.getUsername().contains("[")) {
            return false;
        }
        return MissionManager.super.shouldStopExecution(childTaskFinish);
    }

    @Override
    public void preMission() {
        clickContinueRunnable = new ClickContinueListener.ClickContinueBuilder().withPersisntentOnly().build();
        clickContinueRunnable.start();
    }

    @Override
    public void postMission() {
        if (clickContinueRunnable != null) {
            clickContinueRunnable.terminate();
        }
    }
}
